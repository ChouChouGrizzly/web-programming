// -----------------------------------------------------------
// Assignment #2_Question #2
// Written by: Sasa Zhang (25117151)
// For SOEN 287 - CC - Summer 2019
// -----------------------------------------------------------
 
// Function to validate the user's input string.
function isAnagram() {
    var isAnagram = false;
    var size = 10;   // Maximum array size.
    var numOfUndefined = 0;
    inputArray = [];

    //Prompt boxes for user input.
    for (var i=0; i<size; i++) {
        // Taking input from user.
        inputArray[i] = prompt ("Please enter a pair of string data: " + (i+1));
      
        // Input validation process.
        // If the input is not a null, not a number, not -1.
        if ((inputArray[i] !== " ") && (isNaN(inputArray[i])) && (inputArray[i] != -1)) {
            size++;
            continue;
        }

        // If the input is -1 which termintes the prompt.
        else if (inputArray[i] == -1) {
            break;
        }

        // If the input is a number, then check for number of invalid entry limit.
        else {
            if (i >= 4) { 
                numOfUndefined = 1;
                size++;
                var value = inputArray.pop();   // Pop out the number just entered.

                // Check for previous 4 elements in the array.
                for (var j=i-1; j>i-5; j--) {
                    // If the previous element is a string.
                    if ((inputArray[j] !== undefined) && (isNaN(inputArray[j])) && (inputArray[j] !== -1)) {
                        numOfUndefined = 0;
                        break;
                    }             
                    // If previous element is a number.
                    else if ((inputArray[j] == undefined) || ! (isNaN(inputArray[j]))) {
                        numOfUndefined = numOfUndefined + 1; 
                    }
                }  

                if (numOfUndefined == 5) {
                    document.getElementById("statsOut").innerHTML = "You have exceeded invalid entry limit.";
                    break;
                }    
            } 
        }  
    }

    if (numOfUndefined == 0 ) {   // Beginning of if (numOfUndefined == 0) statement.
        document.getElementById("statsOut").innerHTML = "You have entered below mentioned string pairs.";
       
    //    Check if each string pair entered is abagran or not. 
        for (i=0; i<inputArray.length; i+=2) {
            if ((inputArray[i] !== undefined && isNaN(inputArray[i])) && (inputArray[i+1] !== undefined && isNaN(inputArray[i+1]))) {
                original1=inputArray[i];
                original2=inputArray[i+1];
                string1 = inputArray[i].toUpperCase().split('').sort().join('');
                string2 = inputArray[i+1].toUpperCase().split('').sort().join('');

                if (string1 === string2) {
                    document.getElementById("statsOut").innerHTML += "<br/>Pairs "+ original1 +" and " + original2 + " are anagrams.";
                }
                else {
                    document.getElementById("statsOut").innerHTML += "<br/>Pairs "+ original1 +" and " + original2 + " are not anagrams.";
                }
            }
        }   // End of for-loop.
    }   // End of if(numOfUndefined == 0) statement.
}  // End of function isAnagram().

