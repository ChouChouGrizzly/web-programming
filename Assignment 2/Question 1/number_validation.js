// -----------------------------------------------------------
// Assignment #2_Question #1
// Written by: Sasa Zhang (25117151)
// For SOEN 287 - CC - Summer 2019
// -----------------------------------------------------------
 

// Function to make sure the amount entered is numeric, alert a message otherwise.
function validateNumber() {
  var dom=document.getElementById("numEntered");
  numEntered=dom.value;

  // If the amount entered is not numeric.
  if(isNaN(numEntered)) {
    alert("Please enter numeric value !");
  }
  // If the amount entered is numeric, we continue to test if it is a prime number by calling the funtion isPrimeNumber().
  else {
    numEntered=parseInt(numEntered);
    if (numEntered >= 0) {
      isPrimeNumber();
    }
    else {
      alert("This is not a prime number !");
    }
    
  }
}


// Function to test if the number entered is a prime or not.
function isPrimeNumber() {
  var isPrime = true;
  if(numEntered == 2 || numEntered == 3) {
    isPrime = true;
  }
  else if(numEntered == 1 || numEntered == 0) {
    isPrime = false;
  }
    
  else if(numEntered > 3) {
    for(let i = 2; i < numEntered; i++) {
      if(numEntered%i == 0) {
        isPrime=false;
        break;
      }
    }
  }
   
  // If the number entered is a prime, we continue to call the function reverseNumber() to reverse the prime and display the reversed number.
  if(isPrime) {
    reverseNumber();
    alert("This is a prime number and reverse is: " + result + ".");
  }
  // If the number entered is not a prime, alert the user that this is not a prime.
  else {
    alert("This is not a prime number !");
  }
}



// Function to reverse the entered prime number and return the reversed number.
function reverseNumber() {
     var digit, position = 0;
 
  // If the number has just one digit, simply return the number itself.
   if (numEntered < 10) {
      result = numEntered;
   }

   // If the number has two or more digits, we need to separate each digit in a reverse order.
  else {
      // Get the first digit
      result = numEntered % 10;
      numEntered = Math.floor(numEntered / 10);
  
      // Loop to produce the result for the rest
      do {
          digit = numEntered % 10;
          result = 10 * result + digit;
          numEntered = Math.floor(numEntered / 10);
      } while (numEntered >= 1);
  }

  // Return the reversed prime number.
  return result;
}
