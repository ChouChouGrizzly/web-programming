﻿ <!-- ------------------------------------------------------------------------------
 Assignment 3
 Written by: Sasa Zhang (25117151)
 For SOEN 287 Section CC – Summer 2019
 -----------------------------------------------------------------------------
 Validate data from registration page. -->
<?php session_start();?>
<html>
  <head>
    <style type = "text/css">
      body {background-color:grey; background-image: url(BrownBear.jpg); background-repeat:repeat-x; background-size:650px 600px; margin-left:450px; font-weight:bold; font-size: 20px; color: white;}
    </style>
  </head> 

  <body>  
    <?php
    // define variables 
      $error=FALSE;
      $fnameErr = $emailErr = $unameErr = $pswErr =$rpswErr = "";
      $fname = $e =$email = $u = $uname = $p = $psw = $rpsw = "";
      //check full name
      if (empty($_POST["fname"])) {
        $fnameErr = "Name is required";$error=TRUE;
      } else {
        $fname = $_POST["fname"];
      }
      //check email
      if (empty($_POST["email"])) {
        $emailErr = "Email is required";$error=TRUE;
      } else {
        $e = $_POST["email"];
        // check if e-mail address is gmail or hotmail
        if (preg_match("/@gmail/i",$e)||preg_match("/@hotmail/i",$e)) {
          $email = $_POST["email"]; 
        }else {
          $emailErr = "Invalid email format"; $error=TRUE;
        }
      }
      //check username
      if (empty($_POST["uname"])) {
        $unameErr = "Username is required";$error=TRUE;
      } else {
        $u = $_POST["uname"];
        if (preg_match("/^[a-zA-Z_]+\w+$/",$u)) {
          $uname = $_POST["uname"]; 
        }else {
          $unameErr = "Invalid username"; $error=TRUE;
        }
      }
      //check password
      if (empty($_POST["psw"])) {
        $pswErr = "Password is required";$error=TRUE;
      } else {
        $p = $_POST["psw"];
        if (preg_match("/\W+/",$p)&&preg_match("/[0-9]+/",$p)&&preg_match("/[a-z]+/",$p)&&preg_match("/[A-Z]+/",$p)&&(strlen($p)>=8)) {
        $psw = $_POST["psw"]; 
        }else {
          $pswErr = "Invalid password format";$error=TRUE;}
      }
      //check repeat password
      if (empty($_POST["rpsw"])) {
        $rpswErr = "Repeat password is required";$error=TRUE;} 
      else {
        if ($_POST['psw']!==$_POST['rpsw']) {
          $rpswErr = "Passwords are not identical";$error=TRUE;}
        else {
          $rpsw = $_POST["rpsw"];}
      }

      if($error){
        echo "<h2>Please go back and fill out all these entries:</h2>";
        echo $fnameErr;
        echo "<br>";
        echo $emailErr;
        echo "<br>";
        echo $unameErr;
        echo "<br>";
        echo $pswErr;
        echo "<br>";
        echo $rpswErr;
      }else{
        echo "<h2>A Warm Welcome !</h2>";
        echo "Your full name is: " . $fname;
        echo "<br />";
        echo "Your email address is: " . $email;
        echo "<br />";
        echo "Your username is: " . $uname;
        echo "<br />";
        echo "Your password is: " . $psw;
        echo "<br />";
        echo "Your repeated password is: " . $rpsw;}

      //validate username for question 2
      $testFile=fopen("users.txt","rw") or die("Unable to open file!");
      $find=False;
      while(!feof($testFile)){
        $line=stream_get_line($testFile,1024," ");
        if($uname==$line){$find=TRUE;}}
        if($find){
          echo '<script type="text/javascript"> window.open("registration.html","_self");</script>';  
        }else{
          $_SESSION["admin"] = "user1";
        }
      fclose($testFile);
    ?>
  </body>
</html>


