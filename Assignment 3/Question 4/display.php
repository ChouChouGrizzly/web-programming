<!--
	Assignment 3
	Written by: Sasa Zhang (25117151)
	For SOEN 287 Section CC - Summer 2019
-->

<html>
	<head>
		<title>Number of Visits</title>
		<meta charset = "utf-8" />

		<style>
			body {background-color:grey; background-image: url(BrownBear.jpg); background-repeat:repeat-x; background-size:650px 600px; font-weight: 20px;}
			#progressbar {background-color: gray; border-radius: 13px; padding: 3px;}
			#progressbar>div {background-color: pink; width: 0%; height: 20px; border-radius: 10px;}
			#time {font-size: 200%; margin-left: 600px;}
		</style>

		<script>
			window.onload = () => {min = 0; sec = 60; total = 1 * 60;
			var timer = setInterval(() => {sec--; index = Math.floor(Math.random() * 7);
				if (sec === 0 && min == 0) 
					{clearInterval(timer);
					alert("The cookies have been deleted.");
				} else if (sec === 0) {sec = 59; min--;}

			document.getElementById("time").innerText = ("0" + (min || 0)).slice(-2) + ":" + ("0" + (sec || 0)).slice(-2);
			// var count = 100 - (((min * 60) + sec) / total * 100);
			var count = (((min * 60) + sec) / total * 100);
			document.getElementById("bar").style.width = count + "%";
				}, 1000);
			}
</script>


	</head>

	<body>
		<?php
			/* checking cookie exists or not */
			if (!isset($_COOKIE['count'])) {
				unset($_COOKIE["count"]);
				/* initial cookie value */
				$cookie = 0;
				/* setting cookie with initial value 1 */
				setcookie("count", $cookie, time()+60, "/");
            } else {
				/* incrementing counter value */
				$cookie = ++$_COOKIE['count'];
				/* updating cooking value */
				setcookie("count", $cookie, time()+60, "/");
				/* printint initial counter value */
				echo "<b>You have visited this page ", $cookie ," times.</b><br />";
            }
		?>

		<span id="time"></span>
		<div id="progressbar">
		<div id="bar"></div>
		</div>
   </body>
</html>