<!--
	Assignment 3
	Written by: Sasa Zhang (25117151)
	For SOEN 287 Section CC - Summer 2019
-->


<?php
	session_start();
	$inputToFormat = $_SESSION['inputToFormat'];
	$fontSize = $_SESSION['fontSize'];
	$controlColor = $_SESSION['controlColor'];
	$isVisible = $_SESSION['isVisible'] == 1 ? "visible":"hidden";

	// split the each input to format value and store in array
	$myArray = explode(',', $inputToFormat);

	// Print the stylesheet with the values from the SESSION
	echo "<style type='text/css'>";

	// print each input value like this
	// input[type=text],input[type=submit],input[type=password]
	foreach($myArray as $my_Array) {
		echo "input[type=".$my_Array."],";
	}

	// this echo is by default to manage one extra comma
	echo "input[type=hidden]";
	echo "{";
	echo "font-size: ".$fontSize.";";
	echo "background-color: ".$controlColor.";";
	echo "visibility: ".$isVisible;
	echo "}\n";
	echo "body{background-color:".$controlColor.";}";
	echo "</style>";
?>

<html>
	<head>
		<style type = "text/css">
			body {text-align:center; font-weight:bold; font-size: 30px;}
		</style>
	</head>
	<body>
		<form>
			<input type="text" placeholder="Mobile number"/><br/>
			<input type="password" placeholder="PIN"/><br/>
			<input type="submit" value="Login"/>	
		</form>
	</body>
</html>