<!--
	Assignment 3
	Written by: Sasa Zhang (25117151)
	For SOEN 287 Section CC - Summer 2019
-->


<?php
	//If the form is submitted with values, save them in SESSION and redirect to process.php
	if(isset($_POST['Submit'])) {
		session_start();

		$_SESSION['inputToFormat'] = $_POST['inputToFormat'];
		$_SESSION['fontSize'] = $_POST['fontSize'];
		$_SESSION['controlColor'] = $_POST['controlColor'];
		$_SESSION['isVisible'] = $_POST['isVisible'] == "true";

		header('Location: process.php');
		exit();
	}
	else{
		echo "Welcome, Let's get started!<br /><br />";
	}
?>

<html>
	<head>
		<style type = "text/css">
			body {background-color:grey; background-image: url(BrownBear.jpg); background-repeat:repeat-x; background-size:650px 600px; text-align:center; font-weight:bold; font-size: 30px;}
		</style>

	</head>

	<body>

		<form action="display.php" method="POST">
			<!--This line of code enables of selecting multiple control to stylize-->
			<!-- <label>Control to Stylize: </label><input type="text" name="inputToFormat"/><span>Enter control names separated by comma (e.g. text, password, submit)</span><br/> -->
			<label> Control to stylize
				<select name = "inputToFormat">
					<option value = "text"> text </option>
					<option value = "submit"> submit </option>
					<option value = "password"> password </option>
				</select>
			</label><br /><br />

			<label>Font Size: </label> <input type="range" name="fontSize" min="10" max="30" value="15" /><br /><br />

			<label>Control Color: <input type="color" name="controlColor" value="#e66465" /><br /><br />

			<label>Visible: <input type="checkbox" name="isVisible" checked="checked" value="true"/><br /><br />

			<input type="Submit" name="Submit" value="Submit"/>

		</form>
	</body>
</html>